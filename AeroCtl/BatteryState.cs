﻿namespace AeroCtl;

public enum BatteryState
{
	NoBattery,
	AC,
	DC,
}